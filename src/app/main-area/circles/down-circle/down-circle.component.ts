import { Component } from '@angular/core';
import { MovementService } from '../../../shared/movement.service';
import { GameService } from '../../../shared/game.service';
import { sleep } from '../../../models/constants';

@Component({
  selector: 'app-down-circle',
  templateUrl: './down-circle.component.html',
  // styleUrls: ['./down-circle.component.css']
  styleUrls: ['../circles.css']
})
export class DownCircleComponent {

  audio = new Audio('../../../assets/sounds/sound 4.mp3');

  myId: number = 2;
  pressedSignd: string = 'far';

  constructor(private movementService: MovementService,
    private gameService: GameService) {
    this.movementService.signCurrectMovementObservable.subscribe((whoNeedsToBlink: number) => {
      this.blink(whoNeedsToBlink);
    });
    
  }

  clicked() {
    this.movementService.nextStep().subscribe(currectId => {
      this.gameService.checkMovement(currectId, this.myId);
    });
  }

  async blink(whoNeedsToBlink: number) {
    if (whoNeedsToBlink === this.myId) {
      this.pressed();
      await sleep(500);

      this.unpressed();
      await sleep(200);
    }
  }

  unpressed() {
    this.pressedSignd = 'far';
  }

  pressed() {
    this.audio.play();
    this.pressedSignd = 'fas';
  }
}
