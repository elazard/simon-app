import { Component } from '@angular/core';
import { MovementService } from '../../../shared/movement.service';
import { GameService } from '../../../shared/game.service';
import { sleep } from '../../../models/constants';

@Component({
  selector: 'app-up-circle',
  templateUrl: './up-circle.component.html',
  // styleUrls: ['./up-circle.component.css']
  styleUrls: ['../circles.css']
})
export class UpCircleComponent {

  audio = new Audio('../../../assets/sounds/sound 1.mp3');

  myId: number = 1;
  pressedSignd: string = 'far';
  
  constructor(private movementService: MovementService,
              private gameService:     GameService) {
                this.movementService.signCurrectMovementObservable.subscribe((whoNeedsToBlink: number) => {
                  this.blink(whoNeedsToBlink);
                });
              }

  clicked() {
    this.movementService.nextStep().subscribe(currectId => {
      this.gameService.checkMovement(currectId, this.myId);
    });
  }
  
  async blink(whoNeedsToBlink: number) {
    if (whoNeedsToBlink === this.myId) {
      this.pressed();
      await sleep(500);
      
      this.unpressed(); 
      await sleep(200);
    }
  }

  unpressed() {
    this.pressedSignd = 'far';
  }

  pressed() {
    this.audio.play();
    this.pressedSignd = 'fas';
  }
}
