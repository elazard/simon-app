import { Component } from '@angular/core';
import { MovementService } from '../shared/movement.service';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-main-area',
  templateUrl: './main-area.component.html',
  styleUrls: ['./main-area.component.css']
})
export class MainAreaComponent {

  goButton: boolean = true;
  disableButtons: boolean = true;

  constructor(private movementService: MovementService,
    private gameService: GameService) {

    gameService.endGameObservable.subscribe({
      next: (val) => { this.prepareNewGame() }
    })
  }

  prepareNewGame() {
    this.movementService.setAll();
    this.goButton = true;
  }

  startGame() {
    this.goButton = false;

    this.movementService.randNextMovement();
    this.movementService.showNextMovment();

    this.movementService.disableButtonsObservable.subscribe({
      next: (x) => { this.disableButtons = x; },
      error: (err) => { console.error('something wrong occurred: ' + err); },
      complete: () => { console.log('done'); }
    })
  }
}
