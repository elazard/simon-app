import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainAreaComponent } from './main-area/main-area.component';
import { UpCircleComponent } from './main-area/circles/up-circle/up-circle.component';
import { DownCircleComponent } from './main-area/circles/down-circle/down-circle.component';
import { LeftCircleComponent } from './main-area/circles/left-circle/left-circle.component';
import { RightCircleComponent } from './main-area/circles/right-circle/right-circle.component';
import { MovementService } from './shared/movement.service';

@NgModule({
  declarations: [
    AppComponent,
    MainAreaComponent,
    UpCircleComponent,
    DownCircleComponent,
    LeftCircleComponent,
    RightCircleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [MovementService],
  bootstrap: [AppComponent]
})
export class AppModule { }
