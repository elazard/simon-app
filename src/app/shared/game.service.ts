import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  endGame = new Subject<boolean>();
  endGameObservable = this.endGame.asObservable();

  constructor() { }

  checkMovement(currectMove: number, currentMove: number) {
    if (currectMove !== currentMove) {
      this.endGame.next(true);
    } else {
      return (currectMove === currentMove);
    }
  }
}
