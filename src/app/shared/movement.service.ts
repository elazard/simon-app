import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { sleep } from '../models/constants';

@Injectable({
  providedIn: 'root'
})
export class MovementService {
  /*
    ID:
      up    - red    - 1
      down  - yellow - 2
      left  - green  - 3
      right - blue   - 4
  */
  fullMovementArray: Array<number> = [];
  playerMovementArray: Array<number> = [];
  movementCounter: number = 0;
  currectId: Observable<number>;
  signCurrectMovement = new Subject<any>();
  disableButtons = new Subject<boolean>();

  signCurrectMovementObservable = this.signCurrectMovement.asObservable();
  disableButtonsObservable = this.disableButtons.asObservable();

  setAll() {
    this.fullMovementArray = [];
    this.movementCounter = 0;
  }

  randNextMovement() {
    this.fullMovementArray.push(Math.floor((Math.random() * 4) + 1));
  }

  nextStep() {
    this.currectId = new Observable(observer => {
      observer.next(this.playerMovementArray.shift());
      if (!this.playerMovementArray.length) {
        this.randNextMovement();
        this.showNextMovment();
        this.movementCounter++;

      }
    });
    return this.currectId;
  }

  async showNextMovment() {
    this.disableButtons.next(true);
    await sleep(1000);
    for (let index = 0; index < this.fullMovementArray.length; index++) {
      this.signCurrectMovement.next(this.fullMovementArray[index]);
      await sleep(1000);
    }
    this.disableButtons.next(false);
    this.playerMovementArray = this.fullMovementArray.slice();
  }
}
